import { PopoverController, LoadingController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { ActivatedRoute } from "@angular/router";
import { UserInfo } from "./../main/main.page";
import { Component, OnInit } from "@angular/core";
import { UserDetailsComponent } from "../components/user-details/user-details.component";
import { HttpClient } from "@angular/common/http";
import { LoadingServiceService } from "../services/loading-service.service";
import { AlertServiceService } from "./../services/alert-service.service";
import { LoaderComponent } from "./../components/loader/loader.component";
import { Screenshot } from "@ionic-native/screenshot/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { FilePath } from "@ionic-native/file-path/ngx";

@Component({
  selector: "app-offline-verification",
  templateUrl: "./offline-verification.page.html",
  styleUrls: ["./offline-verification.page.scss"],
})
export class OfflineVerificationPage implements OnInit {
  userInfo: UserInfo = {
    firstName: "",
    lastName: "",
    contactNo: "",
    email: "",
    password: "",
    birthDate: "",
    gender: "",
    parentsContactNo: "",
    standard: "",
    state: "",
    district: "",
    taluka: "",
    appId: "",
    deviceId: "",
    distributerId: "",
    appKey: "",
    regiDate: "",
    expireDate: "",
    status: "",
    activated: "",
    imageData: ""
  };

  deviceId: string = "";
  retry: number = 0;
  disableVerificationField: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private storage: Storage,
    private http: HttpClient,
    private screenShot: Screenshot,
    private filePath: FilePath,
    private socialSharing: SocialSharing,
    private popOverController: PopoverController,
    private loadingController: LoadingController,
    private loadingServiceService: LoadingServiceService,
    private alertServiceService: AlertServiceService
  ) {}

  ngOnInit() {
    this.storage.ready().then((ready) => {
      if (ready) {
        this.storage.get("userInfo").then((data) => {
          this.userInfo = data;
        });
      }
    });
  }

  async offlineRegister() {
    this.retry += 1;
    if (this.retry <= 3) {
      // console.log("offLineRegister", this.userInfo);
      let appId = this.userInfo.appKey.substr(1, 1);
      let distributerId = this.userInfo.appKey.substr(15, 2);
      let deviceId =
        this.userInfo.appKey.substr(2, 2) +
        this.userInfo.appKey.substr(6, 1) +
        this.userInfo.appKey.substr(8, 1) +
        this.userInfo.appKey.substr(11, 1) +
        this.userInfo.appKey.substr(13, 1) +
        this.userInfo.appKey.substr(17);
      if (
        appId != "1" ||
        distributerId != this.userInfo.distributerId ||
        deviceId != this.userInfo.deviceId
      )
        this.alertServiceService.createAlert("Verification key is incorrect");
      else {
        this.getExpiryDate();
        const popover = await this.popOverController.create({
          component: UserDetailsComponent,
          componentProps: {
            userInfo: this.userInfo,
          },
          cssClass: "myPopOver",
          animated: true,
        });
        await popover.present();
      }
    } else {
      this.alertServiceService.createAlert("Too many attempts, contact to your distributer")
      this.disableVerificationField = true;
    }
  }

  getExpiryDate() {
    let year =
      this.userInfo.appKey.substr(5, 1) + this.userInfo.appKey.substr(7, 1);
    let month = this.userInfo.appKey.substr(0, 1);
    if (month == "A") month = "01";
    else if (month == "B") month = "02";
    else if (month == "C") month = "03";
    else if (month == "D") month = "04";
    else if (month == "E") month = "05";
    else if (month == "F") month = "06";
    else if (month == "G") month = "07";
    else if (month == "H") month = "08";
    else if (month == "I") month = "09";
    else if (month == "J") month = "10";
    else if (month == "K") month = "11";
    else if (month == "L") month = "12";
    let date =
      this.userInfo.appKey.substr(10, 1) + this.userInfo.appKey.substr(12, 1);
    this.userInfo.expireDate = year + month + date;
  }

  onKeyUp(ev) {
    let value = this.userInfo.appKey;
    value = value.split("-").join("");
    let finalValue = value.match(/.{1,4}/g).join('-');
    this.userInfo.appKey = finalValue;
  }

  takeScreenShot() {
    this.screenShot.save().then((data) => {
      this.socialSharing.share(
        `${this.userInfo.firstName} ${this.userInfo.lastName}`,
        "",
        `file:///${data.filePath}`
      );
    });
  }
}
