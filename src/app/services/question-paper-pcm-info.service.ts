import { DatabaseServiceService } from "./database-service.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class QuestionPaperPcmInfoService {
  constructor(private databaseServiceService: DatabaseServiceService) {}

  getQuestionPaperYears() {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT DISTINCT QUESTION_YEAR FROM question_paper_pcm_info ORDER BY QUESTION_YEAR DESC`,
        []
      )
      .then((result) => {
        let questionYear = [];
        if (result.rows.length > 0) {
          for (let i = 0; i < result.rows.length; i++) {
            questionYear.push(result.rows.item(i).QUESTION_YEAR);
          }
          return questionYear;
        }
      });
  }

  getPhyQuestionCount(year) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT COUNT(*) as phyQuestion FROM question_paper_pcm_info WHERE SUBJECT_ID = 1 AND QUESTION_YEAR = ?`,
        [year]
      )
      .then((result) => {
        let phyQuestionCount: number;
        if (result.rows.length > 0) {
          phyQuestionCount = result.rows.item(0).phyQuestion;
          return phyQuestionCount;
        }
      });
  }

  getChemQuestionCount(year) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT COUNT(*) as chemQuestion FROM question_paper_pcm_info WHERE SUBJECT_ID = 2 AND QUESTION_YEAR = ?`,
        [year]
      )
      .then((result) => {
        let chemQuestionCount: number;
        if (result.rows.length > 0) {
          chemQuestionCount = result.rows.item(0).chemQuestion;
          return chemQuestionCount;
        }
      });
  }

  getBiologyQuestionCount(year) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT COUNT(*) as biologyQuestion FROM question_paper_pcm_info WHERE SUBJECT_ID = 3 AND QUESTION_YEAR = ?`,
        [year]
      )
      .then((result) => {
        let biologyQuestionCount: number;
        if (result.rows.length > 0) {
          biologyQuestionCount = result.rows.item(0).biologyQuestion;
          return biologyQuestionCount;
        }
      });
  }
}
