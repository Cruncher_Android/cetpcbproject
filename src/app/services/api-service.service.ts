import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  apiUrl: string = "https://cruncher-spring-boot.herokuapp.com/"
  constructor() { }
}
