import { TestBed } from '@angular/core/testing';

import { QuestionBookmarkService } from './question-bookmark.service';

describe('QuestionBookmarkService', () => {
  let service: QuestionBookmarkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionBookmarkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
