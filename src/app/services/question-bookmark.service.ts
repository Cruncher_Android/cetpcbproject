import { DatabaseServiceService } from './database-service.service';
import { Injectable, QueryList } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuestionBookmarkService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  insertBookmarkQue(questions, length, tableName) {
    for(let i = 0; i < length; i++) {
     this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO ${tableName}
      (QUESTION_ID, CHAPTER_ID)values(?,?)`, [questions[i].questionId, questions[i].chapterId]).then(result => {
        // console.log('question added');
        // console.log(result);
        })
    }
    
  }

  removeBookmarkQue (questions, tableName) {
    return this.databaseServiceService.getDataBase().executeSql(`DELETE FROM ${tableName} where QUESTION_ID IN (${questions.toString()})`, [])
    .then(result => {
      // console.log('deleted successfully');
      // console.log(result)
    })
  }
  
  selectId(questionIds, tableName) {
    // console.log('in selectAll')
    return this.databaseServiceService.getDataBase().executeSql(`SELECT * FROM ${tableName} where QUESTION_ID IN 
    (${questionIds.toString()})`,[]).then( result => {
    // console.log('result length in service', result.rows.length);
    let bookmarkQueIds = [];
    if(result.rows.length > 0) {
      for(let i = 0; i < result.rows.length; i++) {
        bookmarkQueIds.push({
          questionId: result.rows.item(i).QUESTION_ID,
          chapterId: result.rows.item(i).CHAPTER_ID
        })
      }
    }
    return bookmarkQueIds;
  })
}

  getBookmarkQueId(chapterIds, tableName) {
     return this.databaseServiceService.getDataBase().executeSql(`SELECT * FROM ${tableName} where CHAPTER_ID IN (${chapterIds.toString()}) `, [])
    .then(result => {
      // console.log(result);
      // console.log('in getBookmarkQueId');
      const questions = [];
      if (result.rows.length > 0) {
        console.log('in if')
        for (let i = 0; i < result.rows.length; i++) {
          // questions.push(result.rows.item(i).QUESTION_ID);
          questions.push({
            questionId: result.rows.item(i).QUESTION_ID,
            chapterId: result.rows.item(i).CHAPTER_ID
          })
        }
        // console.log('questions', questions);
      }
      return questions;
    });
  }
}
