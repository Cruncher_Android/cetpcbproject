import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class StateListService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  getAllStates() {
    // console.log('in get all states')
    return this.databaseServiceService.getDataBase().executeSql('SELECT * FROM STATE_INFO',[]).then(result => {
      // console.log('result in user-dateils', result)
      const stateList = [];
      if (result.rows.length > 0) {
        for (let i = 0; i < result.rows.length; i++) {
          stateList.push({
            stateId: result.rows.item(i).STATE_ID,
            stateName: result.rows.item(i).STATE_NAME
          })
        }
      }
      return stateList;
    })
  }
}
