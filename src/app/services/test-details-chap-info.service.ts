import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class TestDetailsChapInfoService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS testdetails_chap_info
        (chid integer primary key,teste1 integer,testm1 integer,testh1 integer,testt1 integer,testn1 integer,teste2 integer,
          testm2 integer,testh2 integer,testt2 integer,testn2 integer,teste3 integer,testm3 integer,testh3 integer,testt3 integer,
          testn3 integer,teste4 integer,testm4 integer,testh4 integer,testt4 integer,testn4 integer,teste5 integer,testm5 integer,
          testh5 integer,testt5 integer,testn5 integer)`, []);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [19,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [22,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [23,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [24,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [26,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [27,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [29,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [35,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [36,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [37,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [38,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [39,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [43,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [44,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [45,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [46,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [47,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [49,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [50,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [51,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [52,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [54,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [55,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [56,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [57,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [58,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [59,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [61,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [62,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [65,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [66,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [67,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [68,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [69,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [70,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [72,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [73,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [74,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [75,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [76,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [77,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [78,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [79,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_chap_info
        (chid,teste1,testm1,testh1,testt1,testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,
          testm4,testh4,testt4,testn4,teste5,testm5,testh5,testt5,testn5)values
          (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);

      }
    });
  }
}
