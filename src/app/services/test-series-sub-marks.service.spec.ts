import { TestBed } from '@angular/core/testing';

import { TestSeriesSubMarksService } from './test-series-sub-marks.service';

describe('TestSeriesSubMarksService', () => {
  let service: TestSeriesSubMarksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestSeriesSubMarksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
