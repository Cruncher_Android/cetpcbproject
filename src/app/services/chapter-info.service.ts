import { DatabaseServiceService } from './database-service.service';
import { Injectable } from '@angular/core';
import { promise } from 'protractor';



@Injectable({
  providedIn: 'root'
})
export class ChapterInfoService {


  constructor(private databaseServiceService: DatabaseServiceService) { }
  
  selectChapter(subjectId) {
    return this.databaseServiceService.getDataBase().executeSql('SELECT * FROM chapter_info where subject_id = ?', [subjectId])
      .then(result => {
        const chapterList = [];
        if (result.rows.length > 0) {
          for (let i = 0; i < result.rows.length; i++) {
            chapterList.push({
              chapter_id: result.rows.item(i).chapter_id,
              subject_id: result.rows.item(i).subject_id,
              chapter_name: result.rows.item(i).chapter_name,
            });
          }
        }
        return chapterList;
      });
  }

  getChapterName(chapterIds) {
    // console.log(chapterIds.toString())
     return this.databaseServiceService.getDataBase().executeSql(`SELECT chapter_id, chapter_name, subject_id FROM chapter_info WHERE chapter_id IN 
     (${chapterIds.toString()})`, []).then(result => {
       const chapterNames = [];
       if(result.rows.length > 0) {
         for (let i = 0; i < result.rows.length; i++) {
           chapterNames.push({
             chapterId: result.rows.item(i).chapter_id,
             chapterName: result.rows.item(i).chapter_name,
             subjectId: result.rows.item(i).subject_id
           })
         }
       }
       return chapterNames;
    })
  }

}
