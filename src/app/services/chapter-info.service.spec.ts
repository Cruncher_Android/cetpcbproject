import { TestBed } from '@angular/core/testing';

import { ChapterInfoService } from './chapter-info.service';

describe('ChapterInfoService', () => {
  let service: ChapterInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChapterInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
