import { TestBed } from '@angular/core/testing';

import { SaveTestInfoService } from './save-test-info.service';

describe('SaveTestInfoService', () => {
  let service: SaveTestInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaveTestInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
