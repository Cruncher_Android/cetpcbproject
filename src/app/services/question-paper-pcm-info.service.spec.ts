import { TestBed } from '@angular/core/testing';

import { QuestionPaperPcmInfoService } from './question-paper-pcm-info.service';

describe('QuestionPaperPcmInfoService', () => {
  let service: QuestionPaperPcmInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionPaperPcmInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
