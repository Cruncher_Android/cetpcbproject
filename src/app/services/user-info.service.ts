import { Injectable } from "@angular/core";
import { DatabaseServiceService } from "./database-service.service";
import { UserInfo } from "./../main/main.page";

@Injectable({
  providedIn: "root",
})
export class UserInfoService {
  constructor(private databaseServiceService: DatabaseServiceService) {}

  insertIntoTable(userInfo: UserInfo) {
    let user = [
      userInfo.firstName,
      userInfo.lastName,
      userInfo.contactNo,
      userInfo.email,
      userInfo.birthDate,
      userInfo.gender,
      userInfo.parentsContactNo,
      userInfo.standard,
      userInfo.state,
      userInfo.district,
      userInfo.taluka,
      userInfo.password,
      userInfo.appId,
      userInfo.appKey,
      userInfo.deviceId,
      userInfo.distributerId,
      userInfo.expireDate,
      userInfo.regiDate,
      userInfo.status,
      userInfo.imageData,
    ];
    return this.databaseServiceService.getDataBase().executeSql(
      `insert into user_info(f_name, l_name, contact_no, emailId, DOB, gender, parents_contact_no, class, state, district, taluka, passwords, appId, appKey, deviceId, distributerId, expireDate, regiDate, status, imageData) 
      values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      user
    );
  }

  insertIntoTableForDemo(userInfo) {
    let user = [
      userInfo.firstName,
      userInfo.lastName,
      userInfo.contactNo,
      userInfo.email,
    ];
    return this.databaseServiceService.getDataBase().executeSql(
      `insert into user_info(f_name, l_name, contact_no, emailId) 
      values (?,?,?,?)`,
      user
    );
  }

  getUserInfo() {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(`SELECT * FROM user_info`, [])
      .then((result) => {
        if (result.rows.length > 0) {
          let userInfo = {
            firstName: result.rows.item(0).f_name,
            lastName: result.rows.item(0).l_name,
            contactNo: result.rows.item(0).contact_no,
            email: result.rows.item(0).emailId,
            birthDate: result.rows.item(0).DOB,
            gender: result.rows.item(0).gender,
            parentsContactNo: result.rows.item(0).parents_contact_no,
            standard: result.rows.item(0).class,
            state: result.rows.item(0).state,
            district: result.rows.item(0).district,
            taluka: result.rows.item(0).taluka,
            password: result.rows.item(0).passwords,
            appId: result.rows.item(0).appId,
            appKey: result.rows.item(0).appKey,
            deviceId: result.rows.item(0).deviceId,
            distributerId: result.rows.item(0).distributerId,
            expireDate: result.rows.item(0).expireDate,
            regiDate: result.rows.item(0).regiDate,
            status: result.rows.item(0).status,
            activated: "",
            imageData: result.rows.item(0).imageData,
          };
          return userInfo;
        }
      });
  }

  getFirstName() {
    return this.databaseServiceService
      .getDataBase()
      .executeSql("select f_name from user_info", [])
      .then((result) => {
        return result.rows.item(0).f_name;
      });
  }

  
  getUserImage() {
    return this.databaseServiceService
      .getDataBase()
      .executeSql("select imageData from user_info", [])
      .then((result) => {
        return result.rows.item(0).imageData;
      });
  }

    updateUserImage(imageData) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql("update user_info set imageData = ?", [imageData]);
  }


  deleteFromUserInfo() {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(`delete from user_info`, [])
      .then((_) => {
        // this.databaseServiceService
        //   .getDataBase()
        //   .executeSql(`select * from user_info`, [])
        //   .then((data) => console.log("user info", data));
      });
  }
}
