import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class TestDetailsPcbInfoService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS testdetails_PCB_info
        (sbid integer primary key,teste1 integer,testm1 integer,testh1 integer,testt1 integer,testn1 integer,teste2 integer,
          testm2 integer,testh2 integer,testt2 integer,testn2 integer,teste3 integer,testm3 integer,testh3 integer,testt3 integer,
          testn3 integer,teste4 integer,testm4 integer,testh4 integer,testt4 integer,testn4 integer,teste5 integer,testm5 integer,
          testh5 integer,testt5 integer,testn5 integer`, []).then( _ => {
            this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_PCB_info(sbid,teste1,testm1,testh1,testt1,
              testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,testm4,testh4,testt4,testn4,teste5,
              testm5,testh5,testt5,testn5)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
              [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
            this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_PCB_info(sbid,teste1,testm1,testh1,testt1,
              testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,testm4,testh4,testt4,testn4,teste5,
              testm5,testh5,testt5,testn5)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
              [2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
            this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testdetails_PCB_info(sbid,teste1,testm1,testh1,testt1,
              testn1,teste2,testm2,testh2,testt2,testn2 ,teste3,testm3,testh3,testt3,testn3,teste4,testm4,testh4,testt4,testn4,teste5,
              testm5,testh5,testt5,testn5)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
              [4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
          });
        }
    });
  }
}
