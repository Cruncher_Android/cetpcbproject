import { TestBed } from '@angular/core/testing';

import { TestSeriesPcmMarksService } from './test-series-pcm-marks.service';

describe('TestSeriesPcmMarksService', () => {
  let service: TestSeriesPcmMarksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestSeriesPcmMarksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
