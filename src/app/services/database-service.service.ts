import { Injectable } from "@angular/core";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite/ngx";
import { SQLitePorter } from "@ionic-native/sqlite-porter/ngx";
import { Platform } from "@ionic/angular";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class DatabaseServiceService {
  private dataBase: SQLiteObject;
  private dbready: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite,
    private http: HttpClient
  ) {
    // this.plt.ready().then(() => {
    //   this.sqlite.create({
    //     name: 'database2.db',
    //     location: 'default'
    //   }).then((db: SQLiteObject) => {
    //     this.dataBase = db;
    //     this.dbready.next(true);
    //     // this.loadChapters();
    //   });
    // });
    // console.log('in database-service');
  }

  createDataBase() {
    return this.plt.ready().then(() => {
      this.sqlite
        .create({
          name: "PCBDatabase-Updated.db",
          location: "default",
          createFromLocation: 1,
        })
        .then((db: SQLiteObject) => {
          this.dataBase = db;
          this.dbready.next(true);
          // console.log("in then ");
          // db.executeSql("SELECT * FROM subjects", []).then((data) => {
          //   console.log("database service", data);
          // });
        });
    });
  }

  seedDatabse() {
    this.http
      .get("../assets/seed.sql", { responseType: "text" })
      .subscribe((sql) => {
        this.sqlitePorter
          .importSqlToDb(this.dataBase, sql)
          .then((_) => {})
          .catch((e) => console.error(e));
      });
  }

  getDatabaseState() {
    return this.dbready.asObservable();
  }

  getDataBase() {
    return this.dataBase;
  }

  // loadChapters() {
  //   console.log('in loadChapters');
  //   this.getDatabaseState().subscribe( ready => {
  //     if(ready) {
  //       console.log('ready');
  //       this.dataBase.executeSql('CREATE TABLE IF NOT EXISTS chapter_info(chapter_id integer primary key,subject_id integer, chapter_name varchar(255))', []);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[1,1,'Units,Dimensions and Measurment']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[2,1,'Vectors']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[3,1,'Motion in One Dimension']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[4,1,'Motion in Two Dimensions']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[5,1,'Laws of Motion']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[7,1,'Work,Energy and Power']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[8,1,'Centre of Mass,Conservation of Linear Momentum and Collision']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[9,1,'Rotational Dynamics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[10,1,'Gravitaion']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[11,1,'Elasticity']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[12,1,'Fluid Mechanics,Surface Tension and Viscosity']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[13,1,'Thermal Expansion,Calorimetry and Transmission of Heat']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[14,1,'Kinetic Theory of Gases and Thermodynamics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[15,1,'Oscillation and Simple Harmonic Motion']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[16,1,'Wave and Acoustics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[17,1,'Electric Charge and Field']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[18,1,'Electric Potential and Capacitance']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[19,1,'Electric Resistance and Simple Circuits']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[20,1,'Heating Effect of Current and Electrical Measuring Instruments']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[21,1,'Source and Effects of Magnetic Field']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[22,1,'Magnetism and Matter']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[23,1,'E.M. Induction and A.C.Current']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[25,1,'Electromagnetic Waves']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[26,1,'Geometrical Optics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[27,1,'Wave Optics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[28,1,'Dual Nature of Radiation and Matter']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[29,1,'Atomic Structure']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[30,1,'Nuclear Physics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[31,1,'Electronic Devices']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[32,1,'Communication Systems']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[33,2,'Some Basic Concepts in Chemistry']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[34,2,'States Of Matter(Gaseous and Liquid States)']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[35,2,'Atomic Structure']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[36,2,'Chemical Bonding and Molecular Structure']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[37,2,'Chemical Thermodynamics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[38,2,'Equilibrium']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[39,2,'Solid State']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[40,2,'Solutions']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[41,2,'Redox Reactions']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[42,2,'Electrochemistry']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[43,2,'Chemical Kinetics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[44,2,'Surface Chemistry']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[45,2,'Nuclear Chemistry']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[46,2,'Classification of Elements and Periodicity in Properties']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[47,2,'General Principles and Processes of Isolation of Metals']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[48,2,'Hydrogen']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[49,2,'s-Block Elements']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[50,2,'p-Block Elements(Group 13 and 14)']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[51,2,'p-Block Elements(Group 15 to 18)']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[52,2,'d- and f-Block Elements']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[53,2,'Coordination Compounds']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[54,2,'Environmental Chemistry']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[55,2,'Purification and Characterisation of Organic Compounds']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[56,2,'Some Basic Principles of Organic Chemistry']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[57,2,'Hydrocarbons']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[58,2,'Organic Compounds Containing Halogens']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[59,2,'Alcohols,Phenols and Ethers']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[60,2,'Aldehydes,Ketones and Carboxylic Acids']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[61,2,'Organic Compounds Containing Nitrogen']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[62,2,'Polymers']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[63,2,'Biomolecules']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[64,2,'Chemistry in Everyday Life']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[65,2,'Principles Related to Practical Chemistry']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name)values(?,?,?)',[66,3,'Sets']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[67,3,'Relations']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[68,3,'Functions']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[69,3,'Complex Numbers']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[70,3,'Quadratic Equation']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[71,3,'Permutations and Combinations']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[72,3,'Mathematical Induction']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[73,3,'Bionomial Theorem']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[74,3,'Sequences and Series']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[75,3,'Cartesian System of Rectangular Co-ordinates and Straight Lines']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[76,3,'Family of Lines']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[77,3,'Circles and Systems of Circles']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[78,3,'Parabola']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[79,3,'Ellipse']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[80,3,'Hyperbola']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[81,3,'Statistics']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[82,3,'Trigonometric Ratios,Identities and Equations']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[83,3,'Solution of Triangles']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[84,3,'Heights and Distances']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[85,3,'Mathematical Reasoning']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[86,3,'InverseTrigonometric Functions']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[87,3,'Determinants']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[88,3,'Matrices']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[89,3,'Real Numbers']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[90,3,'Limit and Continuity']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[91,3,'Differentiability and Differentiation']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[92,3,'Application of Derivatives']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[93,3,'Indefinite Integrals']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[94,3,'Definite Integral']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[95,3,'Area Under Curves']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[96,3,'Differential Equation']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[97,3,'Three Dimensional Geometry']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[98,3,'Vector Algebra']);
  //       this.dataBase.executeSql('INSERT or IGNORE INTO chapter_info(chapter_id,subject_id,chapter_name) VALUES(?,?,?)',[99,3,'Probability']);
  //   }
  // })

  // }

  // selectChapter(subjectId) {
  //    return this.dataBase.executeSql('SELECT * FROM chapter_info where subject_id = ?', [subjectId])
  //   .then( result => {
  //     const chapterList = [];
  //     if (result.rows.length > 0) {
  //       for (let i = 0; i < result.rows.length; i++) {
  //         chapterList.push({
  //           chapter_id: result.rows.item(i).chapter_id,
  //           subject_id: result.rows.item(i).subject_id,
  //           chapter_name: result.rows.item(i).chapter_name,
  //         });
  //       }
  //     }
  //     return chapterList;
  //   });
  // }
}
