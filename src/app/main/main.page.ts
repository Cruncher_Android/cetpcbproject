import { ForgotPasswordComponent } from "../components/forgot-password/forgot-password.component";
import { LoadingServiceService } from "../services/loading-service.service";
import {
  AlertController,
  PopoverController,
  LoadingController,
} from "@ionic/angular";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { Component, OnInit } from "@angular/core";
import { Device } from "@ionic-native/device/ngx";
import { Network } from "@ionic-native/network/ngx";
import { Dialogs } from "@ionic-native/dialogs/ngx";
import { HttpClient } from "@angular/common/http";
import { AlertServiceService } from "./../services/alert-service.service";
import { UserInfoService } from "./../services/user-info.service";
import { StorageServiceService } from "./../services/storage-service.service";
import { UserDetailsComponent } from "../components/user-details/user-details.component";
import { DemoRegisterationComponent } from "./../components/demo-registeration/demo-registeration.component";
import { InternetServiceService } from "./../services/internet-service.service";
import { ApiServiceService } from "./../services/api-service.service";
import { NgForm } from "@angular/forms";

export interface UserInfo {
  firstName: string;
  lastName: string;
  contactNo: string;
  email: string;
  password: string;
  birthDate: string;
  gender: string;
  parentsContactNo: string;
  standard: string;
  state: string;
  district: string;
  taluka: string;
  deviceId: string;
  appKey: string;
  appId: string;
  distributerId: string;
  regiDate: string;
  expireDate: string;
  status: string;
  activated: string;
  imageData: any;
}

export interface LoginInfo {
  email: string;
  password: string;
  deviceId: string;
  distributerId: string;
  appId: string;
}

@Component({
  selector: "app-main",
  templateUrl: "./main.page.html",
  styleUrls: ["./main.page.scss"],
})
export class MainPage implements OnInit {
  uuid: string = "";
  serialNo: string = "";
  randomNo: string = "";

  offLineRegistration = false;
  checkBox = true;
  deviceId: string = "";

  userInfo: UserInfo = {
    firstName: "",
    lastName: "",
    contactNo: "",
    email: "",
    password: "",
    birthDate: "",
    gender: "",
    parentsContactNo: "",
    standard: "",
    state: "",
    district: "",
    taluka: "",
    appId: "5",
    deviceId: "",
    distributerId: "11",
    appKey: "",
    regiDate: "",
    expireDate: "",
    status: "",
    activated: "",
    imageData: "",
  };
  loginInfo: LoginInfo = {
    email: "",
    password: "",
    deviceId: "",
    distributerId: "",
    appId: "",
  };

  date: string = "";
  month: string = "";
  year: string = "";
  passwordMatch: boolean;
  reTypePass: string = "";

  inputType: string = "password";
  iconName: string = "eye";
  passInputType: string = "password";
  passIconName: string = "eye";
  reTypePassInputType: string = "password";
  reTypePassIconName: string = "eye";

  databaseReady: boolean;

  connected: boolean;

  stopInterval: any;

  time: string = "";

  ShowEmailIdError: boolean = false;

  constructor(
    private device: Device,
    private network: Network,
    private dialogs: Dialogs,
    private storage: Storage,
    private router: Router,
    private alertController: AlertController,
    private popoverController: PopoverController,
    private loadingController: LoadingController,
    private http: HttpClient,
    private loadingServiceService: LoadingServiceService,
    private alertServiceService: AlertServiceService,
    private userInfoService: UserInfoService,
    public storageService: StorageServiceService,
    private internetServiceService: InternetServiceService,
    private apiService: ApiServiceService
  ) {}

  ngOnInit() {
    // this.network.onDisconnect().subscribe((_) => {
    //   this.connected = false;
    // });
    // this.network.onConnect().subscribe((_) => {
    //   this.connected = true;
    // });
  }

  ionViewDidEnter() {
    this.createDeviceId();
  }

  createDeviceId() {
    this.uuid = "";
    this.serialNo = "";
    this.deviceId = "";
    this.uuid = this.device.uuid;
    this.serialNo = this.device.serial;
    // console.log(this.uuid);
    // console.log(this.serialNo);
    // this.date = new Date().getDate().toString();
    // if (this.date.length == 1) this.date = "0" + this.date;
    // this.month = (new Date().getMonth() + 1).toString();
    // if (this.month.length == 1) this.month = "0" + this.month;
    // this.year = new Date().getFullYear().toString().substr(2);

    // this.userInfo.regiDate =
    //   this.date + "-" + this.month + "-" + "20" + this.year;

    // if (this.month == "01") this.month = "A";
    // if (this.month == "02") this.month = "B";
    // if (this.month == "03") this.month = "C";
    // if (this.month == "04") this.month = "D";
    // if (this.month == "05") this.month = "E";
    // if (this.month == "06") this.month = "F";
    // if (this.month == "07") this.month = "G";
    // if (this.month == "08") this.month = "H";
    // if (this.month == "09") this.month = "I";
    // if (this.month == "10") this.month = "J";
    // if (this.month == "11") this.month = "K";
    // if (this.month == "12") this.month = "L";

    // if (this.uuid != "unknown") {
    for (let i = 0; i < this.uuid.length; i = i + 2) {
      this.deviceId = this.deviceId + this.uuid.substr(i, 1);
    }
    this.deviceId = this.deviceId.toUpperCase();
    this.userInfo.deviceId = this.deviceId;
    // let key1 = this.deviceId.substr(0, 2) + this.userInfo.distributerId;
    // let key2 =
    //   this.deviceId.substr(2, 1) +
    //   this.date.substr(0, 1) +
    //   this.deviceId.substr(3, 1) +
    //   this.date.substr(1, 1);
    // let key3 =
    //   this.deviceId.substr(4, 1) +
    //   this.year.substr(0, 1) +
    //   this.deviceId.substr(5, 1) +
    //   this.year.substr(1, 1);
    // let key4 = this.deviceId.substr(6, 2) + this.month + this.userInfo.appId;
    // this.userInfo.deviceId = key1 + "-" + key2 + "-" + key3 + "-" + key4;
    // console.log("userInfo.deviceId", this.userInfo.deviceId);
    //  }
    //else if (this.serialNo != "unknown") {
    //   this.storage.ready().then((ready) => {
    //     if (ready) {
    //       this.storage.get("serialNo").then((data) => {
    //         if (!data) {
    //           if (this.serialNo.length < 16) {
    //             let diff = 16 - this.serialNo.length;
    //             for (let i = 0; i < diff; i++) {
    //               this.serialNo =
    //                 this.serialNo + Math.floor(Math.random() * 10).toString();
    //             }
    //             for (let i = 0; i < this.serialNo.length; i = i + 2) {
    //               this.deviceId = this.deviceId + this.serialNo.substr(i, 1);
    //             }
    //             this.deviceId = this.deviceId.toUpperCase();
    //             console.log("this.deviceId", this.deviceId);
    //             this.storage.ready().then((ready) => {
    //               if (ready) {
    //                 this.storage.set("serialNo", this.deviceId);
    //               }
    //             });
    //             let key1 =
    //               this.deviceId.substr(0, 2) +
    //               this.userInfo.distributerId.substr(0, 1);
    //             let key2 =
    //               this.userInfo.distributerId.substr(1, 1) +
    //               this.deviceId.substr(2, 1) +
    //               this.date.substr(0, 1) +
    //               this.deviceId.substr(3, 1) +
    //               this.date.substr(1, 1);
    //             let key3 =
    //               this.deviceId.substr(4, 1) +
    //               this.year.substr(0, 1) +
    //               this.deviceId.substr(5, 1);
    //             let key4 =
    //               this.year.substr(1, 1) +
    //               this.deviceId.substr(6, 2) +
    //               this.month +
    //               this.userInfo.appId;
    //             this.userInfo.deviceId =
    //               key1 + "-" + key2 + "-" + key3 + "-" + key4;
    //             console.log("userInfo.deviceId", this.userInfo.deviceId);
    //           }
    //         }
    //       });
    //     }
    //   });
    // }
  }

  onRegisterClick() {
    this.userInfo = {
      firstName: "",
      lastName: "",
      contactNo: "",
      email: "",
      password: "",
      birthDate: "",
      gender: "",
      parentsContactNo: "",
      standard: "",
      state: "",
      district: "",
      taluka: "",
      appId: "5",
      deviceId: "",
      distributerId: "11",
      appKey: "",
      regiDate: "",
      expireDate: "",
      status: "",
      activated: "",
      imageData: "",
    };
    this.reTypePass = "";
    this.passwordMatch = null;
    this.storageService.registeration = true;
    this.offLineRegistration = false;
  }

  onLoginClick() {
    this.loginInfo = {
      email: "",
      password: "",
      deviceId: "",
      distributerId: "",
      appId: "",
    };
    this.storageService.registeration = false;
  }

  login(loginForm: NgForm) {
    // this.storage.set("userLoggedIn", true).then((_) => {
    //  this.storageService.userLoggedIn = true;
    // this.router.navigateByUrl("/dashboard/home");
    // })
    if (this.internetServiceService.networkConnected == true) {
      this.loginInfo.distributerId = this.userInfo.distributerId;
      this.loginInfo.appId = this.userInfo.appId;
      this.loadingServiceService.createLoading("Logging in...");
      this.http
        .post(`${this.apiService.apiUrl}loginUpdated`, this.loginInfo)
        .subscribe(
          (data: UserInfo) => {
            this.checkLogin(data, loginForm);
          },
          (err) => {
            this.loginInfo.appId = "1";
            this.http
              .post(`${this.apiService.apiUrl}loginUpdated`, this.loginInfo)
              .subscribe(
                (data: UserInfo) => {
                  this.checkLogin(data, loginForm);
                },
                (err) => {
                  this.loadingController
                    .dismiss()
                    .then((_) =>
                      this.alertServiceService.createAlert(
                        "Email Id or Password is incorrect."
                      )
                    );
                }
              );
          }
        );
    } else if (this.internetServiceService.networkConnected == false) {
      this.alertServiceService.createAlert(
        "Check your internet connection before proceeding to login"
      );
    }
  }

  checkLogin(data, loginForm) {
    this.userInfo = data;
    console.log("user info", this.userInfo);
    console.log("deviceId", this.deviceId);
    if (this.userInfo.deviceId == this.deviceId) {
      this.setLoginStatus(loginForm);
    } else if (this.userInfo.activated == "false") {
      this.loadingController
        .dismiss()
        .then((_) =>
          this.alertServiceService.createAlert(
            "Your account is disabled. Contact to your distributer."
          )
        );
    } else if (this.userInfo.status == "loggedIn") {
      this.loadingController
        .dismiss()
        .then((_) =>
          this.alertServiceService.createAlert("Logout from other devices")
        );
    } else if (
      this.userInfo.status == "loggedOut" ||
      this.userInfo.status == null
    ) {
      this.setLoginStatus(loginForm);
    }
  }

  setLoginStatus(loginForm) {
    this.loginInfo.deviceId = this.deviceId;
    this.http
      .post(
        `${this.apiService.apiUrl}setLoginStatusWithDeviceIdUpdated`,
        this.loginInfo
      )
      .subscribe((_) => {
        this.getExpiryDate();
        this.storage.ready().then((ready) => {
          if (ready) {
            this.storage.set("userLoggedIn", true).then((_) => {
              this.storageService.userLoggedIn = true;
              this.userInfoService.insertIntoTable(this.userInfo).then((_) => {
                this.loadingController.dismiss().then((_) => {
                  loginForm.resetForm();
                  this.loginInfo = {
                    email: "",
                    password: "",
                    deviceId: "",
                    appId: "",
                    distributerId: "",
                  };
                  this.router.navigateByUrl("/dashboard/home");
                });
              });
            });
          }
        });
      }, err => {

      });
  }

  getExpiryDate() {
    let expireDate = this.userInfo.expireDate.split("-");
    this.userInfo.expireDate =
      expireDate[2].substr(2) + expireDate[1] + expireDate[0];
  }

  async offlineRegister() {
    if (this.passwordMatch == true) {
      this.userInfo.deviceId = this.deviceId;
      this.storage.ready().then((ready) => {
        if (ready) {
          // console.log("ready");
          this.storage.set("userInfo", this.userInfo).then(async () => {
            const alert = await this.alertController.create({
              animated: true,
              subHeader:
                "Check your details before proceed for offline registration",
              buttons: [
                {
                  text: "cancel",
                  role: "cancel",
                },
                {
                  text: "Procced",
                  handler: () => {
                    this.handleProceed();
                  },
                },
              ],
            });
            await alert.present();
          });
        }
      });
    } else {
      const alert = await this.alertController.create({
        subHeader: "Enter correct password",
        animated: true,
        buttons: [
          {
            text: "Ok",
            role: "cancel",
          },
        ],
      });
      await alert.present();
    }
  }

  handleProceed() {
    // console.log("handle proceed", this.userInfo);
    if (this.offLineRegistration) {
      this.storage.set("userInfo", this.userInfo).then((_) => {
        this.userInfo = null;
        this.reTypePass = "";
        this.router.navigateByUrl("offline-verification");
      });
    }
  }

  onlineRegister() {
    // const popover = await this.popoverController.create({
    //   component: UserDetailsComponent,
    //   componentProps: {
    //     userInfo: this.userInfo,
    //   },
    //   cssClass: "myPopOver",
    //   animated: true,
    // });
    // await popover.present();
    this.loadingServiceService
      .createLoading("Please wait processing your request...")
      .then((_) => {
        let date = new Date();
        let minutes = date.getMinutes().toString();
        let hours = date.getHours();
        if (hours < 12) this.time = "AM";
        else if (hours > 12) {
          hours = hours - 12;
          this.time = "PM";
        } else if (hours == 12) this.time = "PM";
        if (minutes.length == 1) minutes = "0" + minutes;
        let notiDate =
          date.getDate().toString() +
          "-" +
          (date.getMonth() + 1).toString() +
          "-" +
          date.getFullYear().toString();
        let notiTime = hours.toString() + ":" + minutes + " " + this.time;

        // console.log("onLine register", this.userInfo);
        if (this.internetServiceService.networkConnected) {
          this.userInfo.deviceId = this.deviceId;
          this.http
            .post(
              `${this.apiService.apiUrl}checkRegisterWithDeviceIdAndAppID`,
              {
                email: this.userInfo.email,
                distributerId: this.userInfo.distributerId,
                appId: this.userInfo.appId,
              }
            )
            .subscribe((data) => {
              if (data) {
                this.http
                  .post(`${this.apiService.apiUrl}notify`, {
                    ...this.userInfo,
                    notiDate,
                    notiTime,
                  })
                  .subscribe((data) => {
                    if (data) {
                      this.stopInterval = setInterval(() => {
                        this.checkVerificationKey();
                      }, 2000);
                    }
                  });
              } else if (!data) {
                this.loadingController
                  .dismiss()
                  .then((_) =>
                    this.alertServiceService.createAlert(
                      "Email already registered"
                    )
                  );
              }
            });
        } else if (this.internetServiceService.networkConnected == false) {
          this.loadingController
            .dismiss()
            .then((_) =>
              this.alertServiceService.createAlert(
                "Check your internet connection before proceeding"
              )
            );
        }
      });
  }

  checkVerificationKey() {
    console.log("check verification key", this.userInfo);
    this.http
      .post(`${this.apiService.apiUrl}getUserInfoUpdated`, this.userInfo)
      .subscribe(
        (data: UserInfo) => {
          if (data) {
            clearTimeout(this.stopInterval);
            this.userInfo = data;
            this.getExpiryDate();
            this.loadingController.dismiss().then(async (_) => {
              const popover = await this.popoverController.create({
                component: UserDetailsComponent,
                componentProps: {
                  userInfo: this.userInfo,
                },
                cssClass: "myPopOver",
                animated: true,
              });
              await popover.present();
            });
            this.reTypePass = "";
          }
        }
        // (err) => console.log(err)
      );
  }

  onCheckBoxClick() {
    this.offLineRegistration = !this.offLineRegistration;
    // console.log("offLineStorage", this.offLineRegistration);
    if (this.offLineRegistration == true) {
      if (this.uuid == "unknown" && this.serialNo == "unknown") {
        this.storage.ready().then((ready) => {
          if (ready) {
            // console.log("in ready");
            this.storage.get("randomNo").then((data) => {
              if (!data) {
                this.randomNo = "";
                for (let i = 0; i < 8; i++) {
                  this.randomNo =
                    this.randomNo +
                    this.userInfo.contactNo.charAt(
                      Math.floor(Math.random() * 10)
                    );
                }
                // console.log("randomNo", this.randomNo);
                this.deviceId = this.randomNo;
                this.storage.ready().then((ready) => {
                  if (ready) {
                    this.storage.set("randomNo", this.deviceId);
                  }
                });
                let key1 =
                  this.deviceId.substr(0, 2) +
                  this.userInfo.distributerId +
                  this.deviceId.substr(2, 1);
                let key2 =
                  this.date.substr(0, 1) +
                  this.deviceId.substr(3, 1) +
                  +this.date.substr(1, 1);
                let key3 =
                  this.deviceId.substr(4, 1) +
                  this.year.substr(0, 1) +
                  this.deviceId.substr(5, 1) +
                  this.year.substr(1, 1) +
                  this.deviceId.substr(6, 1);
                let key4 =
                  this.deviceId.substr(7, 1) + this.month + this.userInfo.appId;
                this.userInfo.deviceId =
                  key1 + "-" + key2 + "-" + key3 + "-" + key4;
                // console.log("userInfo.deviceId", this.userInfo.deviceId);
              }
            });
          }
        });
      }
    }
  }

  onKeyUp(event) {
    this.reTypePass = event.target.value;
    if (this.userInfo.password == this.reTypePass) {
      this.passwordMatch = true;
    } else {
      this.passwordMatch = false;
    }
  }

  onPassword() {
    if (this.reTypePass.length > 0) {
      this.reTypePass = "";
      this.passwordMatch = false;
    }
  }

  async onForgotPass() {
    if (this.loginInfo.email == "") this.ShowEmailIdError = true;
    else {
      this.ShowEmailIdError = false;
      const popover = await this.popoverController.create({
        component: ForgotPasswordComponent,
        animated: true,
        cssClass: "myPopOver",
        componentProps: {
          emailId: this.loginInfo.email,
          distributerId: this.userInfo.distributerId,
          appId: this.userInfo.appId,
        },
      });
      await popover.present();
    }
  }

  changeInputType() {
    if (this.inputType == "password") {
      this.inputType = "text";
      this.iconName = "eye-off";
    } else {
      this.inputType = "password";
      this.iconName = "eye";
    }
  }

  changePassInputType() {
    if (this.passInputType == "password") {
      this.passInputType = "text";
      this.passIconName = "eye-off";
    } else {
      this.passInputType = "password";
      this.passIconName = "eye";
    }
  }

  changeReTypePassInputType() {
    if (this.reTypePassInputType == "password") {
      this.reTypePassInputType = "text";
      this.reTypePassIconName = "eye-off";
    } else {
      this.reTypePassInputType = "password";
      this.reTypePassIconName = "eye";
    }
  }

  async onDemoLoginClick() {
    // this.storage.set("userLoggedIn", "demo").then(async (_) => {
    //      this.storageService.userLoggedIn = "demo";
    //      this.router.navigateByUrl("/dashboard/home");
    // })
    if (this.internetServiceService.networkConnected) {
      const popover = await this.popoverController.create({
        component: DemoRegisterationComponent,
        cssClass: "myPopOver",
        animated: true,
      });
      await popover.present();
    } else if (!this.internetServiceService.networkConnected)
      this.alertServiceService.createAlert(
        "Check your internet connection before proceeding"
      );
  }
}
