import { Device } from "@ionic-native/device/ngx";

import { NgModule } from "@angular/core";

import { BrowserModule, HammerModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";

import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

import { SQLite } from "@ionic-native/sqlite/ngx";
import { SQLitePorter } from "@ionic-native/sqlite-porter/ngx";

import { IonicStorageModule } from "@ionic/storage";

import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { HTTP } from '@ionic-native/http/ngx';

import { TestTypeSelectionComponent } from "src/app/components/test-type-selection/test-type-selection.component";

import { LoaderComponent } from "./components/loader/loader.component";

import { Network } from "@ionic-native/network/ngx";
import { Dialogs } from "@ionic-native/dialogs/ngx";
import { SMS } from "@ionic-native/sms/ngx";
import { Chooser } from "@ionic-native/chooser/ngx";
import { Camera } from "@ionic-native/camera/ngx";
import { File } from "@ionic-native/file/ngx";
import { Crop } from "@ionic-native/crop/ngx";
import { FilePath } from "@ionic-native/file-path/ngx";
import { FileChooser } from "@ionic-native/file-chooser/ngx";
import { FileTransfer } from "@ionic-native/file-transfer/ngx";
import { DocumentViewer } from "@ionic-native/document-viewer/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { Screenshot } from "@ionic-native/screenshot/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { UserDetailsComponent } from "./components/user-details/user-details.component";
import { ForgotPasswordComponent } from "./components/forgot-password/forgot-password.component";
import { ChapterListComponent } from "./components/chapter-list/chapter-list.component";
import { ChangeDefaultSettingsComponent } from "./components/change-default-settings/change-default-settings.component";
import { ChangeTestTimeComponent } from "./components/change-test-time/change-test-time.component";
import { DemoRegisterationComponent } from "./components/demo-registeration/demo-registeration.component";

@NgModule({
  declarations: [
    AppComponent,
    TestTypeSelectionComponent,
     UserDetailsComponent,
    ChapterListComponent,
    ChangeDefaultSettingsComponent,
    LoaderComponent,
    ChangeTestTimeComponent,
    DemoRegisterationComponent
  ],
  entryComponents: [
    TestTypeSelectionComponent,
    UserDetailsComponent,
    ChapterListComponent,
    ChangeDefaultSettingsComponent,
    LoaderComponent,
    ChangeTestTimeComponent,
    DemoRegisterationComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HammerModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Device,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    InAppBrowser,
    SQLite,
    SQLitePorter,
    Network,
    Dialogs,
    SMS,
    Chooser,
    Camera,
    File,
    FilePath,
    FileChooser,
    FileTransfer,
    DocumentViewer,
    FileOpener,
    AndroidPermissions,
    HTTP,
    Screenshot,
    SocialSharing,
    Crop
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
