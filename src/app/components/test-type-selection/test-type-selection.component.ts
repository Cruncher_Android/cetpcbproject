import { ChapterInfoService } from "./../../services/chapter-info.service";
import { DatabaseServiceService } from "./../../services/database-service.service";
import { Storage } from "@ionic/storage";
import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import {
  ModalController,
  PopoverController,
  AlertController,
} from "@ionic/angular";
import { ChapterListComponent } from "../chapter-list/chapter-list.component";
import { ChangeDefaultSettingsComponent } from "../change-default-settings/change-default-settings.component";
import { StorageServiceService } from "./../../services/storage-service.service";

@Component({
  selector: "app-test-type-selection",
  templateUrl: "./test-type-selection.component.html",
  styleUrls: ["./test-type-selection.component.scss"],
})
export class TestTypeSelectionComponent implements OnInit {
  @Input() chapters = [];
  @Input() subjectName: string;
  chapterNames: {
    chapterId: number;
    chapterName: string;
    subjectId: number;
  }[] = [];

  defaultSettings: {
    time: number;
    correctMarks: number;
    inCorrectMarks: number;
    questionCount: number;
  };
  manualType = "level";
  test: string = "";
  testType = "automatic";
  disableButton = true;

  cntManual: {
    cntEasy: number;
    cntMedium: number;
    cntHard: number;
    cntNum: number;
    cntTheory: number;
    cntNonAtt: number;
    cntAtt: number;
    cntPrevAsk: number;
  };

  questionsCountForManual: {
    easy: number;
    medium: number;
    hard: number;
    attempted: number;
    nonAttempted: number;
    numerical: number;
    theory: number;
    prevAsk: number;
  };

  questionCount: number;

  manualQuestionCount: number;

  changeCount: any;

  loggedInType: string = "";

  constructor(
    private router: Router,
    private storage: Storage,
    private modalController: ModalController,
    private popOverController: PopoverController,
    private databaseServiceService: DatabaseServiceService,
    private chapterInfoService: ChapterInfoService,
    private storageServiceService: StorageServiceService,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    // console.log("in test type selection");
    this.cntManual = {
      cntEasy: 0,
      cntMedium: 0,
      cntHard: 0,
      cntNum: 0,
      cntTheory: 0,
      cntNonAtt: 0,
      cntAtt: 0,
      cntPrevAsk: 0,
    };

    this.defaultSettings = {
      correctMarks: 0,
      inCorrectMarks: 0,
      questionCount: 0,
      time: 0,
    };

    this.storage.ready().then((ready) => {
      if (ready) {
        this.storage.set("testTypeSelectionPopover", true);
        this.storage.get("userLoggedIn").then((data) => {
          this.loggedInType = data;
          this.getManualQuestionsCount();
        });
      }
    });
    // console.log(this.testType);
    // console.log("chapters in comp", this.chapters);
    this.storage.ready().then((ready) => {
      if (ready) {
        this.storage.get("subject-test").then((result) => {
          if (result == true) {
            this.test = "Subjectwise";
          } else {
            if (this.chapters.length == 1 && this.subjectName != "PCB")
              this.test = "Chapterwise";
            else if (this.chapters.length > 1 && this.subjectName != "PCB")
              this.test = "Unitwise";
            else if (this.subjectName == "PCB") this.test = "Groupwise";
          }
        });

        if (this.storageServiceService.userLoggedIn == true) {
          this.storage.get("defaultSettings").then((result) => {
            // console.log("default settings", result);
            this.defaultSettings = result;
            this.questionCount = this.defaultSettings.questionCount;
          });
        } else if (this.storageServiceService.userLoggedIn == "demo") {
          this.storage.get("defaultSettingsDemo").then((result) => {
            // console.log("default settings", result);
            this.defaultSettings = result;
            this.questionCount = this.defaultSettings.questionCount;
          });
        }
      }
    });

    this.databaseServiceService.getDatabaseState().subscribe((ready) => {
      if (ready) {
        this.chapterInfoService.getChapterName(this.chapters).then((result) => {
          this.chapterNames = result;
          // console.log(result);
        });
      }
    });
  }

  getManualQuestionsCount() {
    this.questionsCountForManual = {
      easy: 0,
      medium: 0,
      hard: 0,
      attempted: 0,
      nonAttempted: 0,
      numerical: 0,
      theory: 0,
      prevAsk: 0,
    };

    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT * FROM question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_LEVEL = 0 AND CHAPTER_ID IN (${this.chapters})`,
        []
      )
      .then((data) => {
        this.questionsCountForManual.easy = data.rows.length;
        // console.log("esay questions", this.questionsCountForManual.easy);
      });

    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT * FROM question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_LEVEL = 1 AND CHAPTER_ID IN (${this.chapters})`,
        []
      )
      .then((data) => {
        this.questionsCountForManual.medium = data.rows.length;
        // console.log("medium questions", this.questionsCountForManual.medium);
      });

    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT * FROM question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_LEVEL = 2 AND CHAPTER_ID IN (${this.chapters})`,
        []
      )
      .then((data) => {
        this.questionsCountForManual.hard = data.rows.length;
        // console.log("hard questions", this.questionsCountForManual.hard);
      });

    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT * FROM question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 1"
            : "ATTEMPTEDVALUE = 1"
        } AND CHAPTER_ID IN (${this.chapters})`,
        []
      )
      .then((data) => {
        this.questionsCountForManual.attempted = data.rows.length;
        // console.log(
        //   "Attempted questions",
        //   this.questionsCountForManual.attempted
        // );
      });

    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT * FROM question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND CHAPTER_ID IN (${this.chapters})`,
        []
      )
      .then((data) => {
        this.questionsCountForManual.nonAttempted = data.rows.length;
        // console.log(
        //   "NonAttempted questions",
        //   this.questionsCountForManual.nonAttempted
        // );
      });

    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT * FROM question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND  QUESTION_TYPE = 1 AND CHAPTER_ID IN (${this.chapters})`,
        []
      )
      .then((data) => {
        this.questionsCountForManual.numerical = data.rows.length;
        // console.log(
        //   "numerical questions",
        //   this.questionsCountForManual.numerical
        // );
      });

    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT * FROM question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND  QUESTION_TYPE = 0 AND CHAPTER_ID IN (${this.chapters})`,
        []
      )
      .then((data) => {
        this.questionsCountForManual.theory = data.rows.length;
        // console.log("theory questions", this.questionsCountForManual.theory);
      });

    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT * FROM question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_YEAR!= '' AND CHAPTER_ID IN (${this.chapters})`,
        []
      )
      .then((data) => {
        this.questionsCountForManual.prevAsk = data.rows.length;
        // console.log("prevAsk questions", this.questionsCountForManual.prevAsk);
      });
  }

  onTestTypeChange() {
    if (this.testType == "automatic") {
      this.defaultSettings.questionCount = this.questionCount;
    } else if (this.testType == "manual") {
      this.manualQuestionCount =
        this.cntManual.cntAtt +
        this.cntManual.cntNonAtt +
        this.cntManual.cntEasy +
        this.cntManual.cntHard +
        this.cntManual.cntMedium +
        this.cntManual.cntNum +
        this.cntManual.cntPrevAsk +
        this.cntManual.cntTheory;
    }
    // console.log(
    //   "default settings question count",
    //   this.defaultSettings.questionCount
    // );
  }

  onChange() {
    // console.log("in on change");
    // console.log("testType", this.testType);
    if (this.testType == "manual") {
      if (this.cntManual.cntEasy > this.questionsCountForManual.easy) {
        this.createAlert();
        this.cntManual.cntEasy = 0;
      } else if (
        this.cntManual.cntMedium > this.questionsCountForManual.medium
      ) {
        this.createAlert();
        this.cntManual.cntMedium = 0;
      } else if (this.cntManual.cntHard > this.questionsCountForManual.hard) {
        this.createAlert();
        this.cntManual.cntHard = 0;
      } else if (
        this.cntManual.cntAtt > this.questionsCountForManual.attempted
      ) {
        this.createAlert();
        this.cntManual.cntAtt = 0;
      } else if (
        this.cntManual.cntNonAtt > this.questionsCountForManual.nonAttempted
      ) {
        this.createAlert();
        this.cntManual.cntNonAtt = 0;
      } else if (
        this.cntManual.cntNum > this.questionsCountForManual.numerical
      ) {
        this.createAlert();
        this.cntManual.cntNum = 0;
      } else if (
        this.cntManual.cntTheory > this.questionsCountForManual.theory
      ) {
        this.createAlert();
        this.cntManual.cntTheory = 0;
      } else if (
        this.cntManual.cntPrevAsk > this.questionsCountForManual.prevAsk
      ) {
        this.createAlert();
        this.cntManual.cntPrevAsk = 0;
      } else {
        this.manualQuestionCount =
          this.cntManual.cntAtt +
          this.cntManual.cntNonAtt +
          this.cntManual.cntEasy +
          this.cntManual.cntHard +
          this.cntManual.cntMedium +
          this.cntManual.cntNum +
          this.cntManual.cntPrevAsk +
          this.cntManual.cntTheory;
      }
    } else if (this.testType == "automatic") {
      this.defaultSettings.questionCount = 30;
    }
  }

  onManualTypeChanged() {
    // console.log("manualtypechanged", this.manualType);
    if (this.manualType == "level") {
      this.cntManual.cntAtt = 0;
      this.cntManual.cntNonAtt = 0;
      this.cntManual.cntNum = 0;
      this.cntManual.cntPrevAsk = 0;
      this.cntManual.cntTheory = 0;
    } else if (this.manualType == "type") {
      this.cntManual.cntEasy = 0;
      this.cntManual.cntMedium = 0;
      this.cntManual.cntHard = 0;
      this.cntManual.cntAtt = 0;
      this.cntManual.cntNonAtt = 0;
      this.cntManual.cntPrevAsk = 0;
    } else if (this.manualType == "attempt") {
      this.cntManual.cntEasy = 0;
      this.cntManual.cntMedium = 0;
      this.cntManual.cntHard = 0;
      this.cntManual.cntNum = 0;
      this.cntManual.cntPrevAsk = 0;
      this.cntManual.cntTheory = 0;
    } else if (this.manualType == "prevAsk") {
      this.cntManual.cntEasy = 0;
      this.cntManual.cntMedium = 0;
      this.cntManual.cntHard = 0;
      this.cntManual.cntNum = 0;
      this.cntManual.cntTheory = 0;
      this.cntManual.cntAtt = 0;
      this.cntManual.cntNonAtt = 0;
    }
  }

  async createAlert() {
    let alert = await this.alertController.create({
      subHeader: "Invalid Count",
      cssClass: "alert-title",
      buttons: [
        {
          text: "Ok",
          role: "cancel",
        },
      ],
    });
    await alert.present();
  }

  startTest() {
    this.storageServiceService.questionCount = this.manualQuestionCount;
    this.storage.ready().then((ready) => {
      if (ready) {
        // if(this.cntManual != null) {
        //   this.storage.set('testMode', this.testType);
        // } else {
        //   this.testType = 'automatic'
        //   this.storage.set('testMode', this.testType);
        // }
        if (this.storageServiceService.userLoggedIn == true)
          this.storage.set("defaultSettings", this.defaultSettings);
        else if (this.storageServiceService.userLoggedIn == "demo")
          this.storage.set("defaultSettingsDemo", this.defaultSettings);
        this.storage.set("testTypeSelectionPopover", false);
        this.storage.set("testMode", this.testType);
        this.storage.set("cntManual", this.cntManual);
        this.storage.set("chapterNames", this.chapterNames);
        this.storage.set("subjectName", this.subjectName);
        this.storage.set("manualType", this.manualType);
        this.storage.set("selectedChapters", this.chapters).then((_) => {
          // console.log("selectedChapters", this.chapters);
          this.router.navigateByUrl("/test");
          this.popOverController.dismiss();
          // console.log("router url", this.router.url);
        });
      }
    });
  }

  onClose() {
    this.storage.set("testTypeSelectionPopover", false);
    this.popOverController.dismiss();
  }

  async showChapters(event) {
    const popover = await this.popOverController.create({
      component: ChapterListComponent,
      animated: true,
      cssClass: "myPopOver1",
      event: event,
      componentProps: {
        chapterNames: this.chapterNames,
        subjectName: this.subjectName,
      },
    });
    await popover.present();
  }

  async onIconClick() {
    const popover = await this.popOverController.create({
      component: ChangeDefaultSettingsComponent,
      animated: true,
      cssClass: "myPopOver1",
      backdropDismiss: false,
      componentProps: {
        defaultSettings: this.defaultSettings,
        testMode: this.testType,
        // settings: this.defaultSettings
      },
    });
    await popover.present();
    popover.onDidDismiss().then((_) => {
      this.storage.ready().then((ready) => {
        if (ready) {
          if (this.storageServiceService.userLoggedIn == true) {
            this.storage.get("defaultSettings").then((result) => {
              this.defaultSettings = result;
              if (this.testType == "automatic")
                this.questionCount = this.defaultSettings.questionCount;
            });
          } else if (this.storageServiceService.userLoggedIn == "demo") {
            this.storage.get("defaultSettingsDemo").then((result) => {
              this.defaultSettings = result;
              if (this.testType == "automatic")
                this.questionCount = this.defaultSettings.questionCount;
            });
          }
        }
      });
    });
  }

  onEasyUp() {
    this.cntManual.cntEasy++;
  }

  onEasyDown() {
    if (this.cntManual.cntEasy > 0) this.cntManual.cntEasy--;
  }

  onMediumUp() {
    this.cntManual.cntMedium++;
  }

  onMediumDown() {
    if (this.cntManual.cntMedium > 0) this.cntManual.cntMedium--;
  }

  onHardUp() {
    this.cntManual.cntHard++;
  }

  onHardDown() {
    if (this.cntManual.cntHard > 0) this.cntManual.cntHard--;
  }

  onNumUp() {
    this.cntManual.cntNum++;
  }

  onNumDown() {
    if (this.cntManual.cntNum > 0) this.cntManual.cntNum--;
  }

  onTheoryUp() {
    this.cntManual.cntTheory++;
  }

  onTheoryDown() {
    if (this.cntManual.cntTheory > 0) this.cntManual.cntTheory--;
  }

  onNonAttUp() {
    this.cntManual.cntNonAtt++;
  }

  onNonAttDown() {
    if (this.cntManual.cntNonAtt > 0) this.cntManual.cntNonAtt--;
  }

  onAttUp() {
    this.cntManual.cntAtt++;
  }

  onAttDown() {
    if (this.cntManual.cntAtt > 0) this.cntManual.cntAtt--;
  }

  onPrevAskUp() {
    this.cntManual.cntPrevAsk++;
  }

  onPrevAskDown() {
    if (this.cntManual.cntPrevAsk > 0) this.cntManual.cntPrevAsk--;
  }

  onEasyUpPress() {
    // console.log("easy up press");
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntEasy >= 0 &&
        this.cntManual.cntEasy < this.questionsCountForManual.easy
      ) {
        this.cntManual.cntEasy += 1;
      }
    }, 50);
  }

  onEasyDownPress() {
    // console.log("easy up press");
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntEasy > 0 &&
        this.cntManual.cntEasy <= this.questionsCountForManual.easy
      ) {
        this.cntManual.cntEasy -= 1;
      }
    }, 50);
  }

  onMediumUpPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntMedium >= 0 &&
        this.cntManual.cntMedium < this.questionsCountForManual.medium
      ) {
        this.cntManual.cntMedium += 1;
      }
    }, 50);
  }

  onMediumDownPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntMedium > 0 &&
        this.cntManual.cntMedium <= this.questionsCountForManual.medium
      ) {
        this.cntManual.cntMedium -= 1;
      }
    }, 50);
  }

  onHardUpPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntHard >= 0 &&
        this.cntManual.cntHard < this.questionsCountForManual.hard
      ) {
        this.cntManual.cntHard += 1;
      }
    }, 50);
  }

  onHardDownPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntHard > 0 &&
        this.cntManual.cntHard <= this.questionsCountForManual.hard
      ) {
        this.cntManual.cntHard -= 1;
      }
    }, 50);
  }

  onNumUpPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntNum >= 0 &&
        this.cntManual.cntNum < this.questionsCountForManual.numerical
      ) {
        this.cntManual.cntNum += 1;
      }
    }, 50);
  }

  onNumDownPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntNum > 0 &&
        this.cntManual.cntNum <= this.questionsCountForManual.numerical
      ) {
        this.cntManual.cntNum -= 1;
      }
    }, 50);
  }

  onTheoryUpPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntTheory >= 0 &&
        this.cntManual.cntTheory < this.questionsCountForManual.theory
      ) {
        this.cntManual.cntTheory += 1;
      }
    }, 50);
  }

  onTheoryDownPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntTheory > 0 &&
        this.cntManual.cntTheory <= this.questionsCountForManual.theory
      ) {
        this.cntManual.cntTheory -= 1;
      }
    }, 50);
  }

  onNonAttUpPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntNonAtt >= 0 &&
        this.cntManual.cntNonAtt < this.questionsCountForManual.nonAttempted
      ) {
        this.cntManual.cntNonAtt += 1;
      }
    }, 50);
  }

  onNonAttDownPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntNonAtt > 0 &&
        this.cntManual.cntNonAtt <= this.questionsCountForManual.nonAttempted
      ) {
        this.cntManual.cntNonAtt -= 1;
      }
    }, 50);
  }

  onAttUpPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntAtt >= 0 &&
        this.cntManual.cntAtt < this.questionsCountForManual.attempted
      ) {
        this.cntManual.cntAtt += 1;
      }
    }, 50);
  }

  onAttDownPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntAtt > 0 &&
        this.cntManual.cntAtt <= this.questionsCountForManual.attempted
      ) {
        this.cntManual.cntAtt -= 1;
      }
    }, 50);
  }

  onPrevAskUpPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntPrevAsk >= 0 &&
        this.cntManual.cntPrevAsk < this.questionsCountForManual.prevAsk
      ) {
        this.cntManual.cntPrevAsk += 1;
      }
    }, 50);
  }

  onPrevAskDownPress() {
    this.changeCount = setInterval(() => {
      if (
        this.cntManual.cntPrevAsk > 0 &&
        this.cntManual.cntPrevAsk <= this.questionsCountForManual.prevAsk
      ) {
        this.cntManual.cntPrevAsk -= 1;
      }
    }, 50);
  }

  onPressup() {
    clearInterval(this.changeCount);
  }
}
