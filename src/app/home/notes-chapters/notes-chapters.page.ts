import { Component, OnInit } from "@angular/core";
import { ChapterInfoService } from "./../../services/chapter-info.service";
import { ActivatedRoute } from "@angular/router";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";

import {
  FileTransfer,
  FileTransferObject,
} from "@ionic-native/file-transfer/ngx";
import { File, FileEntry } from "@ionic-native/file/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";
import {
  InAppBrowser,
  InAppBrowserOptions,
} from "@ionic-native/in-app-browser/ngx";
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: "app-notes-chapters",
  templateUrl: "./notes-chapters.page.html",
  styleUrls: ["./notes-chapters.page.scss"],
})
export class NotesChaptersPage implements OnInit {
  subjectId: string;
  subjectName: string;
  chapterList: {
    chapter_id: number;
    subject_id: number;
    chapter_name: string;
    fileExist: boolean;
    downloading: boolean;
  }[] = [];
  downloading: boolean = false;
  loggedInType: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private chapterInfoService: ChapterInfoService,
    private fileTransfer: FileTransfer,
    private file: File,
    private fileOpener: FileOpener,
    private androidPermission: AndroidPermissions,
    private inAppBrowser: InAppBrowser,
    private alertController: AlertController,
    private storage: Storage
  ) {}

  ngOnInit() {
    this.storage.get("userLoggedIn").then((data) => {
      this.loggedInType = data;
    });
    this.activatedRoute.paramMap.subscribe((subName) => {
      this.subjectId = subName.get("subjectId");
      this.subjectName = subName.get("subjectName");
      this.chapterInfoService.selectChapter(this.subjectId).then((results) => {
        results.map((result) =>
          this.chapterList.push({
            chapter_id: result.chapter_id,
            chapter_name: result.chapter_name,
            subject_id: result.subject_id,
            fileExist: false,
            downloading: false,
          })
        );
        this.file
        .listDir(`${this.file.externalRootDirectory}Preparation`, "CET-PCB")
        .then((fileEntry: FileEntry[]) => {
            fileEntry.map((entry) => {
              if (
                this.chapterList.find(
                  ({ chapter_name }) =>
                    chapter_name === entry.name.split(".")[0]
                )
              )
                this.chapterList.find(
                  ({ chapter_name }) =>
                    chapter_name === entry.name.split(".")[0]
                ).fileExist = true;
            });
            console.log("chapterlist in notes", this.chapterList);
          });
      });
    });
  }

  onDownloadFile(chapterName, index) {
    if(index > 1 && this.loggedInType == 'demo') {
      this.createAlert('Please contact your distributer for complete access');
      return
    }
    this.androidPermission
      .checkPermission(this.androidPermission.PERMISSION.WRITE_EXTERNAL_STORAGE)
      .then((result) => {
        if (!result.hasPermission) {
          this.androidPermission
            .requestPermission(
              this.androidPermission.PERMISSION.WRITE_EXTERNAL_STORAGE
            )
            .then((response) => {
              this.download(chapterName);
            });
        } else {
          this.download(chapterName);
        }
      });
  }

  onOpenFile(chapterName, index) {
    if(index > 1 && this.loggedInType == 'demo') {
      this.createAlert('Please contact your distributer for complete access');
      return
    }
    let path = `${this.file.externalRootDirectory}Preparation/CET-PCB/${chapterName}.pdf`;
    this.fileOpener.open(path, "application/pdf").then(
      (_) => {
        console.log("In opener");
      },
      (err) => console.log(JSON.stringify(err))
    );
  }

  download(chapterName) {
    let path = `${this.file.externalRootDirectory}Preparation/CET-PCB`;
    console.log("path", path);
    this.chapterList.find(
      ({ chapter_name }) => chapter_name === chapterName
    ).downloading = true;
    const transfer: FileTransferObject = this.fileTransfer.create();
    transfer
      .download(
        `http://www.cetonlinetest.com/ChapterNotes/PDF/CET/RoyalAcademyPDF/${chapterName}.pdf`,
        `${path}/${chapterName}.pdf`
      )
      .then(
        (entry) => {
          this.chapterList.find(
            ({ chapter_name }) => chapter_name === chapterName
          ).downloading = false;
          this.chapterList.find(
            ({ chapter_name }) => chapter_name === chapterName
          ).fileExist = true;
          let url = entry.toURL();
          console.log("entry", entry);
          console.log("url", url);
          this.fileOpener.open(url, "application/pdf").then(
            (_) => {
              console.log("In opener");
            },
            (err) => console.log(JSON.stringify(err))
          );
        },
        (err) => console.log(JSON.stringify(err))
      );
  }

  onChapterName(chapterName, fileExists, index) {
    if(index > 1 && this.loggedInType == 'demo') {
      this.createAlert('Please contact your distributer for complete access');
      return
    }
    if (fileExists) {
      this.onOpenFile(chapterName, index);
    } else if (!fileExists) {
      this.inAppBrowser.create(
        `https://docs.google.com/viewer?url=http://www.cetonlinetest.com/ChapterNotes/PDF/CET/RoyalAcademyPDF/${chapterName}.pdf&embedded=true`,
        "_blank",
        { location: "no" }
      );
    }
  }

  async createAlert(message) {
    const alert = await this.alertController.create({
      subHeader: message,
      cssClass: "alert-title",
      animated: true,
      buttons: [
        {
          role: "cancel",
          text: "Ok",
        },
      ],
    });
    await alert.present();
  }
}
