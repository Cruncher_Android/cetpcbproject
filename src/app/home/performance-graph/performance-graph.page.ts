import { DatabaseServiceService } from "./../../services/database-service.service";
import { SaveTestInfoService } from "./../../services/save-test-info.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Chart } from "chart.js";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-performance-graph",
  templateUrl: "./performance-graph.page.html",
  styleUrls: ["./performance-graph.page.scss"],
})
export class PerformanceGraphPage implements OnInit {
  @ViewChild("lineChartPhy") lineChartPhy;
  @ViewChild("lineChartChem") lineChartChem;
  @ViewChild("lineChartBiology") lineChartBiology;
  @ViewChild("lineChartPCB") lineChartPCB;
  @ViewChild("lineChartQue") lineChartQue;
  chartPhy: any;
  chartChem: any;
  chartBiology: any;
  chartPCB: any;
  chartQue: any;

  phySelected: boolean;
  chemSelected: boolean;
  biologySelected: boolean;
  pcbSelected: boolean;
  queSelected: boolean;
  selectedSubject: string;

  testDetailsPhy: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetailsChem: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetailsBiology: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetailsPCB: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetailsQue: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetails: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  loggedInType: string = "";

  constructor(
    private saveTestInfoService: SaveTestInfoService,
    private databaseServiceService: DatabaseServiceService,
    private storage: Storage
  ) {
    // console.log("performance graph page loaded");
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.storage.get("userLoggedIn").then((data) => {
      this.loggedInType = data;
      this.testDetailsPhy = [];
      this.testDetailsChem = [];
      this.testDetailsBiology = [];
      this.testDetailsPCB = [];
      this.testDetailsQue = [];
      this.testDetails = [];
      this.databaseServiceService.getDatabaseState().subscribe((ready) => {
        if (ready) {
          this.saveTestInfoService
            .getSubjectWiseTest(
              "Physics%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetails.push(detail);
                this.testDetailsPhy.push(detail);
              });
              this.phySelected = true;
              this.chemSelected = false;
              this.biologySelected = false;
              this.pcbSelected = false;
              this.queSelected = false;
              this.selectedSubject = "Physics";
              this.createLineChartForPhy();
            });

          this.saveTestInfoService
            .getSubjectWiseTest(
              "Chemistry%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetailsChem.push(detail);
              });
            });

          this.saveTestInfoService
            .getSubjectWiseTest(
              "Biology%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetailsBiology.push(detail);
              });
            });

          this.saveTestInfoService
            .getSubjectWiseTest(
              "PCB%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetailsPCB.push(detail);
              });
            });

          this.saveTestInfoService
            .getSubjectWiseTest(
              "CET%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetailsQue.push(detail);
              });
            });
        }
      });
    });
  }

  onPhyClick() {
    if (!this.phySelected) {
      this.testDetails = [];
      this.testDetailsPhy.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "Physics";
      this.phySelected = true;
      this.chemSelected = false;
      this.biologySelected = false;
      this.pcbSelected = false;
      this.queSelected = false;
      this.createLineChartForPhy();
    }
  }

  onChemClick() {
    if (!this.chemSelected) {
      this.testDetails = [];
      this.testDetailsChem.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "Chemistry";
      this.phySelected = false;
      this.chemSelected = true;
      this.biologySelected = false;
      this.pcbSelected = false;
      this.queSelected = false;
      this.createLineChartForChem();
    }
  }

  onBiologyClick() {
    if (!this.biologySelected) {
      this.testDetails = [];
      this.testDetailsBiology.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "Biology";
      this.phySelected = false;
      this.chemSelected = false;
      this.biologySelected = true;
      this.pcbSelected = false;
      this.queSelected = false;
      this.createLineChartForBiology();
    }
  }

  onPCBClick() {
    if (!this.pcbSelected) {
      this.testDetails = [];
      this.testDetailsPCB.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "PCB";
      this.phySelected = false;
      this.chemSelected = false;
      this.biologySelected = false;
      this.pcbSelected = true;
      this.queSelected = false;
      this.createLineChartForPCB();
    }
  }

  onQueClick() {
    if (!this.queSelected) {
      this.testDetails = [];
      this.testDetailsQue.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "CET";
      this.phySelected = false;
      this.chemSelected = false;
      this.biologySelected = false;
      this.pcbSelected = false;
      this.queSelected = true;
      this.createLineChartForQue();
    }
  }

  createLineChartForPhy() {
    let chartData = [];
    let labels = [];
    this.testDetailsPhy.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsPhy.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartPhy = new Chart(this.lineChartPhy.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
            // backgroundColor: 'rgba(0,0,0,0)'
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });

    // console.log("teastDetailsPhy", this.testDetailsPhy);
  }

  createLineChartForChem() {
    let chartData = [];
    let labels = [];
    this.testDetailsChem.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsChem.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartChem = new Chart(this.lineChartChem.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });

    // console.log("teastDetailsChem", this.testDetailsChem);
  }
  createLineChartForBiology() {
    let chartData = [];
    let labels = [];
    this.testDetailsBiology.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsBiology.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartBiology = new Chart(this.lineChartBiology.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });

    // console.log("teastDetailsBiology", this.testDetailsBiology);
  }
  createLineChartForPCB() {
    let chartData = [];
    let labels = [];
    this.testDetailsPCB.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsPCB.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartPCB = new Chart(this.lineChartPCB.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });
    // console.log("teastDetailsPCB", this.testDetailsPCB);
  }
  createLineChartForQue() {
    let chartData = [];
    let labels = [];
    this.testDetailsQue.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsQue.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartQue = new Chart(this.lineChartQue.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });
    // console.log("teastDetailsQue", this.testDetailsQue);
  }
}
