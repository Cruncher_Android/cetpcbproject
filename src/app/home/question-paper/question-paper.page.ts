import { DatabaseServiceService } from './../../services/database-service.service';
import { QuestionPaperPcmInfoService } from './../../services/question-paper-pcm-info.service';
import { PopoverController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ChangeTestTimeComponent } from 'src/app/components/change-test-time/change-test-time.component';
import { AlertController } from '@ionic/angular';
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-question-paper',
  templateUrl: './question-paper.page.html',
  styleUrls: ['./question-paper.page.scss'],
})
export class QuestionPaperPage implements OnInit {

  questionYears: {
    year: number,
    phyQuestions: number,
    chemQuestions: number,
    biologyQuestions: number
  }[] = [];

  loggedInType: string = '';
  constructor(private inAppBrowser: InAppBrowser,
              private popoverController: PopoverController,
              private questionPaperPcmInfoService: QuestionPaperPcmInfoService,
              private databaseServiceService: DatabaseServiceService,
              private alertController: AlertController,
              private storage: Storage) { 
    // console.log('question paper page loaded');
  }

  ngOnInit() {
    this.storage.get("userLoggedIn").then((data) => {
      this.loggedInType = data;
    });
    this.databaseServiceService.getDatabaseState().subscribe(ready => {
      if(ready) {
        this.questionPaperPcmInfoService.getQuestionPaperYears().then(years => {
          years.map(year => {
            this.questionYears.push({
              year: year,
              phyQuestions: 0,
              chemQuestions: 0,
              biologyQuestions: 0
            })
          })
          this.questionYears.map(year => {
            this.questionPaperPcmInfoService.getPhyQuestionCount(year.year).then(result => {
              year.phyQuestions = result;
            })
            this.questionPaperPcmInfoService.getChemQuestionCount(year.year).then(result => {
              year.chemQuestions = result;
            });
            this.questionPaperPcmInfoService.getBiologyQuestionCount(year.year).then(result => {
              year.biologyQuestions = result;
            });
          })
          // console.log('question years', this.questionYears)
        })
    }
  })
  }

  openInBrowser() {
    this.inAppBrowser.create('https://ionicframework.com/docs/native/in-app-browser');
  }

  async onIconClick(ev) {
    const popover = await this.popoverController.create({
      component: ChangeTestTimeComponent,
      event: ev,
      animated: true,
      backdropDismiss: false,
      cssClass: 'myPopOver1'
    })
    await popover.present();
  }

  async onQuestionPeperClick(index) {
    if(index > 1 && this.loggedInType == 'demo') {
      const alert = await this.alertController.create({
        subHeader: 'Please contact your distributer for complete access',
        cssClass: "alert-title",
        animated: true,
        buttons: [
          {
            role: "cancel",
            text: "Ok",
          },
        ],
      })
      await alert.present()
    }
  }
}
