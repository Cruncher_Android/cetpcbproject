import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnitTestPageRoutingModule } from './unit-test-routing.module';

import { UnitTestPage } from './unit-test.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UnitTestPageRoutingModule
  ],
  declarations: [UnitTestPage]
})
export class UnitTestPageModule {}
