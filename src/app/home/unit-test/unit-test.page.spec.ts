import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UnitTestPage } from './unit-test.page';

describe('UnitTestPage', () => {
  let component: UnitTestPage;
  let fixture: ComponentFixture<UnitTestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitTestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UnitTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
