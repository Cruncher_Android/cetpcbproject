import { StatusBar } from "@ionic-native/status-bar/ngx";
import { SaveTestInfoService } from "./../services/save-test-info.service";
import {
  OverallSummaryService,
  OverAllSummary,
} from "./../services/overall-summary.service";
import { DatabaseServiceService } from "../services/database-service.service";
import { Component } from "@angular/core";
import { OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import { TestLevelService } from "./../services/test-level.service";
import { StorageServiceService } from "./../services/storage-service.service";
import { UserInfoService } from "../services/user-info.service";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {
  overAllSummary: OverAllSummary = {
    chemAttQue: 0,
    chemTotalQue: 0,
    biologyAttQue: 0,
    biologyTotalQue: 0,
    phyAttQue: 0,
    phyTotalQue: 0,
    totalAttQue: 0,
    totalCorrQue: 0,
  };
  overAllAccuracy: number;

  lastTestDetails: {
    testId: number;
    obtainedMarks: number;
    totalMarks: number;
    testType: string;
    testSubmitted: string;
  };

  testLevel: {
    testId: number;
    obtainedMarks: number;
    totalMarks: number;
    percentage: number;
    testType: string;
  };

  bestTest: {
    testId: number;
    obtainedMarks: number;
    totalMarks: number;
    percentage: number;
    testType: string;
  };

  worstTest: {
    testId: number;
    obtainedMarks: number;
    totalMarks: number;
    percentage: number;
    testType: string;
  };

  settings = {
    time: 30,
    correctMarks: 4,
    inCorrectMarks: 1,
    questionCount: 30,
  };

  phyTotalQuestions: number = 6724;
  chemTotalQuestions: number = 5475;
  biologyTotalQuestions: number = 9365;

  phyProgress: number = 0;
  chemProgress: number = 0;
  biologyProgress: number = 0;

  hours: number;
  greetingMsg: string = "";

  testStatus: string = "";

  loggedInType: string = "";

  userName: string = "";

  imageData: string = ""; 

  constructor(
    private databaseServiceService: DatabaseServiceService,
    private overallSummaryService: OverallSummaryService,
    private saveTestInfoService: SaveTestInfoService,
    private testLevelService: TestLevelService,
    private storageServiceService: StorageServiceService,
    private userInfoService: UserInfoService,
    private status: StatusBar,
    private storage: Storage,
    private inAppBrowser: InAppBrowser
  ) {
    // console.log("home page loaded");
  }

  ngOnInit() {
    this.lastTestDetails = {
      testId: 0,
      obtainedMarks: 0,
      totalMarks: 0,
      testType: "",
      testSubmitted: "",
    };

    this.testLevel = {
      testId: 0,
      obtainedMarks: 0,
      totalMarks: 0,
      percentage: 0,
      testType: "",
    };

    this.bestTest = {
      testId: 0,
      obtainedMarks: 0,
      totalMarks: 0,
      percentage: 0,
      testType: "",
    };

    this.worstTest = {
      testId: 0,
      obtainedMarks: 0,
      totalMarks: 0,
      percentage: 0,
      testType: "",
    };

    this.status.backgroundColorByHexString("#3880ff");
    this.setDefaultSettings();
  }

  ionViewDidEnter() {
    // console.log("view did enter");
    this.storage.get("userLoggedIn").then((data) => {
      this.loggedInType = data;
      this.getHomePageDetails();
      this.getGreetings();
    });
  }

  getGreetings() {
    this.hours = new Date().getHours();
    if (this.hours < 12) this.greetingMsg = "morning";
    if (this.hours >= 12 && this.hours < 17) this.greetingMsg = "afternoon";
    if (this.hours >= 17) this.greetingMsg = "evening";
  }

  setDefaultSettings() {
    this.storage.ready().then((ready) => {
      if (ready) {
        if (this.storageServiceService.userLoggedIn == true) {
          this.storage.get("setDefaultSettings").then((data) => {
            if (data == null) {
              this.storage.set("defaultSettings", this.settings).then((_) => {
                this.storage.set("setDefaultSettings", "seted");
              });
            }
          });
        } else if (this.storageServiceService.userLoggedIn == "demo") {
          this.storage.get("setDefaultSettingsDemo").then((data) => {
            if (data == null) {
              this.storage
                .set("defaultSettingsDemo", this.settings)
                .then((_) => {
                  this.storage.set("setDefaultSettingsDemo", "seted");
                });
            }
          });
        }
      }
    });

  }

  getHomePageDetails() {
    this.databaseServiceService.getDatabaseState().subscribe((ready) => {
      if (ready) {
        this.saveTestInfoService
          .getLastTestInfo(
            this.loggedInType == "demo"
              ? "save_test_info_demo"
              : "save_test_info"
          )
          .then((result) => {
            // console.log("last test", result);
            this.lastTestDetails = result;

            this.getOverAllSummary();
          });
      }
    });
  }
  getOverAllSummary() {
    this.overallSummaryService
      .getOverallSummary(
        this.loggedInType == "demo" ? "overall_summary_demo" : "overall_summary"
      )
      .then((result) => {
        // console.log("logged in type", this.loggedInType);
        this.overAllSummary = result;
        // console.log("OverAllSummary", this.overAllSummary);
        if (this.overAllSummary != null) {
          if (this.overAllSummary.totalAttQue == 0) {
            this.overAllAccuracy = 0;
          } else {
            this.overAllAccuracy = Math.round(
              (this.overAllSummary.totalCorrQue /
                this.overAllSummary.totalAttQue) *
                100
            );
          }
          this.phyProgress =
            this.overAllSummary.phyAttQue / this.phyTotalQuestions;
          this.chemProgress =
            this.overAllSummary.chemAttQue / this.chemTotalQuestions;
          this.biologyProgress =
            this.overAllSummary.biologyAttQue / this.biologyTotalQuestions;

          this.testLevel = {
            testId: this.lastTestDetails.testId,
            obtainedMarks: this.lastTestDetails.obtainedMarks,
            totalMarks: this.lastTestDetails.totalMarks,
            percentage: Math.round(
              (this.lastTestDetails.obtainedMarks /
                this.lastTestDetails.totalMarks) *
                100
            ),
            testType: this.lastTestDetails.testType,
          };

          if (this.lastTestDetails.testId > 0) {
            this.databaseServiceService
              .getDatabaseState()
              .subscribe((ready) => {
                if (ready) {
                  if (this.lastTestDetails.testId == 1) {
                    this.testLevelService
                      .updateTest(
                        this.testLevel,
                        this.loggedInType == "demo"
                          ? "BEST_TEST"
                          : "BEST_TEST_DEMO"
                      )
                      .then((_) => {
                        this.bestTest = {...this.testLevel};
                      });

                    this.testLevelService
                      .updateTest(
                        this.testLevel,
                        this.loggedInType == "demo"
                          ? "WORST_TEST"
                          : "WORST_TEST_DEMO"
                      )
                      .then((_) => {
                        this.worstTest = {...this.testLevel};
                      });
                  } else if (this.lastTestDetails.testId > 1) {
                    this.testLevelService
                      .getTest(
                        this.loggedInType == "demo"
                          ? "BEST_TEST"
                          : "BEST_TEST_DEMO"
                      )
                      .then((data) => {
                        this.bestTest = data;
                        if (
                          this.testLevel.percentage >= this.bestTest.percentage
                        ) {
                          this.testLevelService
                            .updateTest(
                              this.testLevel,
                              this.loggedInType == "demo"
                                ? "BEST_TEST"
                                : "BEST_TEST_DEMO"
                            )
                            .then((_) => {
                              this.bestTest = {...this.testLevel};
                            });
                        }
                      });

                    this.testLevelService
                      .getTest(
                        this.loggedInType == "demo"
                          ? "WORST_TEST"
                          : "WORST_TEST_DEMO"
                      )
                      .then((data) => {
                        this.worstTest = data;
                        if (
                          this.testLevel.percentage <= this.worstTest.percentage
                        ) {
                          this.testLevelService
                            .updateTest(
                              this.testLevel,
                              this.loggedInType == "demo"
                                ? "WORST_TEST"
                                : "WORST_TEST_DEMO"
                            )
                            .then((_) => {
                              this.worstTest = {...this.testLevel};
                            });
                        }
                      });
                  }
                }
              });
          } else {
            this.worstTest = {
              obtainedMarks: 0,
              percentage: 0,
              testId: 0,
              testType: "",
              totalMarks: 0,
            };
            this.bestTest = {
              obtainedMarks: 0,
              percentage: 0,
              testId: 0,
              testType: "",
              totalMarks: 0,
            };
          }
        }
      });

    // this.databaseServiceService.getDataBase().executeSql(`SELECT COUNT(*) as phyTotalQue from question_info WHERE SUBJECT_ID = 1`, []).
    // then(result => {
    //   this.phyTotalQuestions = result.rows.item(0).phyTotalQue;
    // })
    // this.databaseServiceService.getDataBase().executeSql(`SELECT COUNT(*) as chemTotalQue from question_info WHERE SUBJECT_ID = 2`, []).
    // then(result => {
    //   this.chemTotalQuestions = result.rows.item(0).chemTotalQue;
    // })
    // this.databaseServiceService.getDataBase().executeSql(`SELECT COUNT(*) as mathsTotalQue from question_info WHERE SUBJECT_ID = 3`, []).
    // then(result => {
    //   this.mathsTotalQuestions = result.rows.item(0).mathsTotalQue;
    // })
    this.userInfoService.getFirstName().then((data) => (this.userName = data));
    this.userInfoService.getUserImage().then((data) => (this.imageData = data));
  }

  onLastTestClick() {
    this.storage.set("page", "saved-test");
  }
  openInBrowser() {
    this.inAppBrowser.create('http://www.scholarskatta.com');
  }
}
